<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/26/15
 * Time: 2:39 PM
 * @var $this Mage_Catalog_Model_Resource_Setup
 */

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_category', 'home_image', array(
    'type'          => 'varchar',
    'label'         => 'Home image',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'sort_order'    => 99,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'General Information'
));

$installer->endSetup();

